import {User, usersArray} from "./users";
import * as userInfo from "./usersInfo";
import {Age, Organization, usersInfoArra} from "./usersInfo";

interface UserPosition extends User,Age{
    position?:string;
}

const getUsersJobPositions = (usersArray:User[])=>{
    const newUsersArray = usersArray.map(
        person => {
           delete person.userid;
           const obj = usersInfoArra.find(item =>{
               if(item.name === person.name){
                   return item;
               }
           });
           // @ts-ignore
            person.age = obj.age
           // @ts-ignore
            person.position = obj.organization.position;
            return person;
           })
   return newUsersArray;
        }

const usersPositions = getUsersJobPositions(usersArray);
console.log('userPositions', usersPositions);
