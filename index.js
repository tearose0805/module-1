import { usersArray } from "./users.js";
import { usersInfoArra } from "./usersInfo.js";
var getUsersJobPositions = function (usersArray) {
    var newUsersArray = usersArray.map(function (person) {
        delete person.userid;
        var obj = usersInfoArra.find(function (item) {
            if (item.name === person.name) {
                return item;
            }
        });
        // @ts-ignore
        person.age = obj.age;
        // @ts-ignore
        person.position = obj.organization.position;
        return person;
    });
    return newUsersArray;
};
var usersPositions = getUsersJobPositions(usersArray);
console.log('userPositions', usersPositions);
//# sourceMappingURL=index.js.map